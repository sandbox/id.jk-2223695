<?php

/**
 * @file
 * Contains \Drupal\message\MessageCategoryInterface.
 */

namespace Drupal\message;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Message category entity.
 */
interface MessageCategoryInterface extends ConfigEntityInterface {

}
