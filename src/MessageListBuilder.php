<?php

/**
 * @file
 * Contains \Drupal\message\MessageListBuilder.
 */

namespace Drupal\message;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\Language;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Defines a class to build a listing of node entities.
 *
 * @see \Drupal\node\Entity\Node
 */
class MessageListBuilder extends EntityListBuilder {
  
  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id())
    );
  }
  
  //public function __construct() {
  //}
}