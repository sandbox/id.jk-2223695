<?php

namespace Drupal\message\Form;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form.
 */
class MessageTextCopy extends FormBase {
  /**
   * @var entityManager.
   */
  protected $languageManager;
  
  /**
   * @var entityManager.
   */
  protected $moduleHandler;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManagerInterface $languageManager, ModuleHandlerInterface $moduleHandler) {
    $this->languageManager = $languageManager;
    $this->moduleHandler = $moduleHandler;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('module_handler')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Only display the form if the locale module is enabled.
    if (!$this->moduleHandler()->moduleExists('locale')) {
      $form['_notice'] = array(
        '#markup' => $this->t('Copying messages text fields is dependent on the locale module.'),
      );
      return $form;
    }
    
    $languages = $this->languageManager()->getLanguages();
    $language_options = array(Language::LANGCODE_NOT_SPECIFIED => t('"Language none"'));
    foreach ($languages as $langcode => $language) {
      $language_options[$langcode] = $language->getName();
    }
    
    $form['origin'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select a language to copy from'),
      '#options' => $language_options,
      '#required' => TRUE,
    );
    
    $form['destinations'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select languages to copy to'),
      '#multiple' => TRUE,
      '#options' => $language_options,
      '#required' => TRUE,
    );
    
    $form['override'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Override existing text'),
      '#description' => $this->t('When checked, existing text in the destination languages will be overriden by the origin language.'),
    );
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Copy'),
    );
  
    return $form;
  }
  
  /**
   * 
   */
  public function languageManager() {
    return $this->languageManager;
  }
  
  /**
   *
   */
  public function moduleHandler() {
    return $this->moduleHandler;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (in_array($form_state->getValue('origin'), $form_state->getValue('destinations'))) {
      $this->setFormError('destinations', $form_state, $this->t('Cannot copy language values into itself.'));
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = array(
      'title' => $this->t('Copying messages texts betweem languages'),
      'operations' => array(
        array(
          'message_admin_text_copy_batch',
          array(
            $form_state->getValue('origin'),
            $form_state->getValue('destinations'),
            $form_state->getValue('override'),
          ),
        ),
      ),
      'init_message' => $this->t('Starting message texts copying.'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('Message texts copying has encountered an error.'),
    );
  
    batch_set($batch);
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'message_admin_text_copy';
  }
}