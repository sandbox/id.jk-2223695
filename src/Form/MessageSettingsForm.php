<?php

namespace Drupal\message\Form;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Settings form.
 */
class MessageSettingsForm extends ConfigFormBase {
  /**
   * @var entityManager.
   */
  protected $entityManager;
  
  /**
   * @var entityManager.
   */
  protected $moduleHandler;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactory $config_factory, EntityManagerInterface $entityManager, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($config_factory);
    $this->entityManager = $entityManager;
    $this->moduleHandler = $moduleHandler;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity.manager'),
      $container->get('module_handler')
    );
  }
  
  /**
   * 
   */
  public function entityManager() {
    return $this->entityManager;
  }
  
  /**
   *
   */
  public function moduleHandler() {
    return $this->moduleHandler;
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('message.settings');
    $form['purge'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Purge settings'),
    );
  
    $form['purge']['message_purge_enable'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Purge messages'),
      '#description' => $this->t('When enabled, old messages will be deleted.'),
      '#default_value' => $config->get('purge.enable'),
    );
  
    $states = array(
      'visible' => array(
        ':input[name="message_purge_enable"]' => array('checked' => TRUE),
      ),
    );
  
    $form['purge']['message_purge_quota'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Messages quota'),
      '#description' => $this->t('Maximal (approximate) amount of messages.'),
      '#default_value' => $config->get('purge.quota'),
      '#states' => $states,
    );
  
    $form['purge']['message_purge_days'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Purge messages older than'),
      '#description' => $this->t('Maximal message age in days.'),
      '#default_value' => $config->get('purge.days'),
      '#states' => $states,
    );
  
    $options = array();
    foreach ($this->entityManager()->getDefinitions() as $entity_id => $entity) {
      $options[$entity_id] = $entity->get('label');
    }
  
    $form['message_delete_on_entity_delete'] = array(
      '#title' => $this->t('Auto delete messages referencing the following entities'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $options,
      '#default_value' =>  $config->get('delete.on_entity_delete'),
      '#description' => $this->t('Messages that reference entities of these types will be deleted when the referenced entity gets deleted.'),
    );
  
    // Display link to the copy form if the locale module is enabled.
    if ($this->moduleHandler()->moduleExists('locale')) {
      $form['links'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Other settings'),
      );
  
      $form['links']['text_copy'] = array(
        '#markup' => $this->l($this->t('Copy messages text fields from one language to others'), Url::fromRoute('message.settings.text_copy')),
      );
    }
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('message.settings')
      ->set('purge.enable', $form_state->getValue('message_purge_enable'))
      ->set('purge.quota', $form_state->getValue('message_purge_quota'))
      ->set('purge.days', $form_state->getValue('message_purge_days'))
      ->set('delete.on_entity_delete', $form_state->getValue('message_delete_on_entity_delete'))
      ->save();
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'message_user_admin_settings';
  }
}