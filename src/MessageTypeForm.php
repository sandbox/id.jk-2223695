<?php

/**
 * @file
 * Contains \Drupal\message\MessageTypeFormController.
 */

namespace Drupal\message;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;
use Drupal\Component\Utility\String;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\message\Entity;

class MessageTypeForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $op = $this->operation;
     //$message_type, $op
    $message_type = $this->entity;
    // @todo create multiple forms basing on this one and just overload needed methods;
    //$op = 'edit';
    if ($op == 'clone') {
      $message_type->description .= ' (cloned)';
      // Save the original message type into form state so that the submit
      // handler can clone its field instances.
      $form_state['original_message_type'] = menu_get_object('entity_object', 4);
    }
    
    $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textfield',
      //'#default_value' => $message_type->description,
      '#description' => t('The human-readable description of this message type.'),
      '#required' => TRUE,
      '#weight' => -5,
    );
    // Machine-readable type name.
    //exit('<pre>' . print_r($message_type, 1) . '</pre>');
    $form['name'] = array(
      '#type' => 'machine_name',
      //'#default_value' => $message_type->name,
      //'#disabled' => $message_type->hasStatus(ENTITY_IN_CODE),
      '#machine_name' => array(
        'exists' => 'message_type_load',
        'source' => array('description'),
      ),
      '#description' => t('A unique machine-readable name for this message type. It must only contain lowercase letters, numbers, and underscores.'),
      '#weight' => -5,
    );
    
    // We might have gotten the message type category via ajax, so set it in the
    // message type entity.
    $message_category = $form_state->getValue('message_category');
    if (!empty($message_category)) {
      $message_type->category = $message_category;
    }
    
    if ($op == 'add') {
      // Get all the message type category, and allow user to choose one using ajax.
      $options = array();
      $options['message_type'] = t('Default message type category');
      //foreach (message_category_load() as $message_category) {
      //  $options[$message_category->category] = !empty($message_category->description) ? $message_category->description : $message_type->category;
      //}
    
      $form['message_category'] = array(
        '#title' => t('Message type category'),
        '#type' => 'select', //count($options) > 1 ? 'select' : 'value',
        '#options' => $options,
        //'#default_value' => $message_type->category,
        '#description' => t('Select the message type category.'),
        '#required' => TRUE,
        '#ajax' => array(
          'callback' => 'message_type_fields_ajax_callback',
          'wrapper' => 'message-type-wrapper',
        ),
      );
      //exit('<pre>' . print_r($form['message_category'], 1) . '</pre>');
    }
    else {
      // @todo.
      if (FALSE /*$message_category = message_category_load($message_type->category)*/) {
        $value = !empty($message_category->description) ? check_plain($message_category->description) : check_plain($message_type->category);
      }
      else {
        $value = t('Default message type category');
      }
      $form['message_category'] = array(
        '#title' => t('Message type category'),
        '#type' => count(array()/*message_category_load()*/) > 1 ? 'item' : 'value',
        '#markup' => $value,
      );
    }
    
    $form['language'] = array(
      '#title' => t('Field language'),
      '#description' => t('The language code that will be saved with the field values. This is used to allow translation of fields.'),
    );
    
    $field_language = NULL;
    
    if (\Drupal::moduleHandler()->moduleExists('locale')) {
      $options = array();
      foreach (language_list() as $key => $value) {
        if (!empty($value->status)) {
          $options[$key] = $value->label;
        }
      }
      $language = $form_state->getValue('language');
      $field_language = !empty($language) ? $language : language_default()->langcode;
      $form['language'] += array(
        '#type' => 'select',
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $field_language,
        '#ajax' => array(
          'callback' => 'message_type_fields_ajax_callback',
          'wrapper' => 'message-type-wrapper',
        ),
      );
    }
    else {
      $form['language'] += array(
        '#type' => 'item',
        '#markup' => t('Undefined language'),
      );
    }
    
    $form['message_type_fields'] = array(
      '#prefix' => '<div id="message-type-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#parents' => array('message_type_fields'),
    );
    
    
    
    
    
    // $form_state['form_display']->buildForm($message_type, $form['message_type_fields'], $form_state);
    //field_attach_form($message_type, $form['message_type_fields'], $form_state, $field_language);
    
    
    
    
    
    
    $token_types = \Drupal::moduleHandler()->moduleExists('entity_token') ? array('message') : array();
    if (!$token_types) {
      $form['entity_token'] = array('#markup' => '<p>' . t('Optional: Enable "Entity token" module to use Message and Message-type related tokens.') . '</p>');
    }
    
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['token_tree'] = array(
        '#theme' => 'token_tree',
        '#token_types' => $token_types + array('all'),
      );
    
    }
    else {
      $form['token_tree'] = array(
        '#markup' => '<p>' . t("Optional: Install <a href='@token-url'>Token</a> module, to show a the list of available tokens.", array('@token-url' => 'http://drupal.org/project/token')) . '</p>',
      );
    }
    
    $params = array(
      '@url-rules' => 'http://drupal.org/project/rules',
      '!link' => 'http://api.drupal.org/api/drupal/includes--bootstrap.inc/function/t/7',
    );
    
    $form['argument_keys'] = array(
      '#title' => t('Replacement tokens'),
      '#type' => 'textfield',
      //'#default_value' => implode(', ', (array) $message_type->argument_keys),
      '#description' => t('Optional: For <a href="@url-rules">Rules</a> module, in order to set argument using Rules actions, a comma-separated list of replacement tokens, e.g. %title or !url, of which the message text makes use of. Each replacement token has to start with one of the special characters "@", "%" or "!". This character controls the sanitization method used, analogously to the <a href="!link">t()</a> function.', $params),
    );
    
    $form['data'] = array(
      // Placeholder for other module to add their settings, that should be added
      // to the data column.
      '#tree' => TRUE,
    );
    
    
    $form['data']['token options']['clear'] = array(
      '#title' => t('Clear empty tokens'),
      '#type' => 'checkbox',
      '#description' => t('When this option is selected, empty tokens will be removed from display.'),
      //'#default_value' => isset($message_type->data['token options']['clear']) ? $message_type->data['token options']['clear'] : FALSE,
    );
    
    $form['data']['purge'] = array(
      '#type' => 'fieldset',
      '#title' => t('Purge settings'),
    );
    
    $form['data']['purge']['override'] = array(
      '#title' => t('Override global settings'),
      '#type' => 'checkbox',
      '#description' => t('Override global purge settings for messages of this type.'),
      //'#default_value' => !empty($message_type->data['purge']['override']),
    );
    
    $states = array(
      'visible' => array(
        ':input[name="data[purge][override]"]' => array('checked' => TRUE),
      ),
    );
    
    $form['data']['purge']['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Purge messages'),
      '#description' => t('When enabled, old messages will be deleted.'),
      //'#default_value' => !empty($message_type->data['purge']['enabled']),
      '#states' => $states,
    );
    
    $states = array(
      'visible' => array(
        ':input[name="data[purge][enabled]"]' => array('checked' => TRUE),
      ),
    );
    
    $form['data']['purge']['quota'] = array(
      '#type' => 'textfield',
      '#title' => t('Messages quota'),
      '#description' => t('Maximal (approximate) amount of messages of this type.'),
      //'#default_value' => !empty($message_type->data['purge']['quota']) ? $message_type->data['purge']['quota'] : '',
      //'#element_validate' => array('element_validate_integer_positive'),
      '#states' => $states,
    );
    
    $form['data']['purge']['days'] = array(
      '#type' => 'textfield',
      '#title' => t('Purge messages older than'),
      '#description' => t('Maximal message age in days, for messages of this type.'),
      //'#default_value' => !empty($message_type->data['purge']['days']) ? $message_type->data['purge']['days'] : '',
      //'#element_validate' => array('element_validate_integer_positive'),
      '#states' => $states,
    );
    
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save message type'),
      '#weight' => 40,
    );
    
    if (FALSE /*!$message_type->hasStatus(ENTITY_IN_CODE) && $op != 'add'*/) {
      $form['actions']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete message type'),
        '#weight' => 45,
        '#limit_validation_errors' => array(),
        '#submit' => array('message_type_form_submit_delete')
      );
    }
    return $form; 
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $entity = parent::buildEntity($form, $form_state);
    return $entity;
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function validate(array $form, FormStateInterface $form_state) {
    $this->buildEntity($form, $form_state);
    parent::validate($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $message_type = $this->entity;
    $message_type->name = trim($message_type->name);
  
    $status = $message_type->save();
  
    $t_args = array('%name' => $message_type->label());
  
    if ($status == SAVED_UPDATED) {
      drupal_set_message(t('The message type %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      drupal_set_message(t('The message type %name has been added.', $t_args));
      $context = array_merge($t_args, array('link' => $this->l(t('View'), new Url('message.overview_types'))));
      //$this->logger('message')->notice('Added message type %name.', $context);
    }
  
    $this->entityManager->clearCachedFieldDefinitions();
    $form_state->setRedirect('message.overview_types');
  }
}

/**
 * Form API submit callback for the delete button.
 */
function message_type_form_submit_delete(&$form, FormStateInterface $form_state) {
  $form_state['redirect'] = 'admin/structure/messages/manage/' . $form_state['message_type']->name . '/delete';
}
